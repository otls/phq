<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'user', 'middleware' => ['jwt'],], function () {
    #auth user
    Route::group(['prefix' => 'auth'], function () {
        Route::post('/', 'Api\Users\AuthController@login')->withoutMiddleware('jwt');
        Route::delete('/', 'Api\Users\AuthController@logout');
        Route::put('/', 'Api\Users\AuthController@refresh');
    });
    #management fishpond configuration
    Route::resource('/config', 'Api\Users\ConfigController')->except('create', 'edit');
    #Fishpond
    Route::group(['prefix' => 'fishpond', 'middleware' => ['fishpond.owner']], function () {
        Route::get('/', 'Api\Users\FishpondController@index')->withoutMiddleware(['fishpond.owner']);
        Route::get('/{fishpond}', 'Api\Users\FishpondController@show');
        Route::put('/{fishpond}/{config}', 'Api\Users\FishpondController@updateConfig')->middleware('config.owner');
    });
    #Device/microcontroller
    Route::group(['prefix' => 'device', 'middleware' => ['device.owner']], function () {
        Route::get('/', 'Api\Users\DeviceController@index')->withoutMiddleware('device.owner');
        Route::get('/{device}', 'Api\Users\DeviceController@index');
    });
});



// Microcontroller ROUTEs
Route::group(['prefix' => 'mc', 'middleware' => ['mc.jwt']], function () {

    Route::group(['prefix' => 'auth'], function () {
        Route::post('/', 'Api\Devices\McAuthController@login')->withoutMiddleware(['mc.jwt', 'mc.fishpond.owner']);
    });

    Route::put('/monitor/fishpond/{fishpond}', 'Api\Devices\McController@fishpondMonitor')->middleware('mc.fishpond.owner');
    Route::get('/config', 'Api\Devices\McController@getConfig');
    Route::get('/config/{fishpond}', 'Api\Devices\McController@getConfigByFishpond')->middleware('mc.fishpond.owner');
});
