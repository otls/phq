<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('front.index');
});

Auth::routes();

Route::group(['prefix' => 'mimin', 'middleware' => ['auth']], function () {

    Route::get('/', 'Admin\HomeController@index')->name('home');
    Route::post('/user/data', 'Admin\UserController@data')->name('user-data');
    Route::resource('/user', 'Admin\UserController');
});
