<?php

use App\Models\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = config('phq.roles.admin.role_code');
        $admin = [
            'name' => 'Mochamad Admin',
            'email' => 'm.admin@mail.com',
            'password' => Hash::make("m-admin123"),
            'role_code' => $role
        ];

        Role::firstOrCreate(['role_code' => $role], config('phq.roles.admin'));
        User::where('email', $admin['email'])->forceDelete();

        User::create($admin);
    }
}
