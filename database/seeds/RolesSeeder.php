<?php

use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles  = config('phq.roles');
        if (Schema::hasTable('roles')) {
            foreach ($roles as $key => $value) {
                Role::firstOrCreate(['role_code' => $value['role_code']], $value);
            }
        } else {
            echo "No roles table, maybe you forget to migrate db";
        }
    }
}
