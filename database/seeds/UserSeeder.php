<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'name' => 'User Maulana',
            'email' => 'user@gmail.com',
            'password' => Hash::make('user123'),
            'role_code' => 110,

        ];
        User::updateOrCreate(
            ['email' => 'user@mail.com'],
            $data
        );
    }
}
