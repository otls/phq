<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserIdColumnToDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('devices')) {
            if (!Schema::hasColumn('devices', 'user_id')) {
                Schema::table('devices', function (Blueprint $table) {
                    $table->bigInteger('user_id', false, true)->nullable()->after('id');
                    $table->foreign('user_id', 'devices_user_id_foreign')->references('id')->on('users')->onDelete('cascade');
                });
            } else {
                echo "User_id column already exsists";
            }
        } else {
            echo "No devices table";
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('devices')) {
            if (Schema::hasColumn('devices', 'user_id')) {
                Schema::table('devices', function (Blueprint $table) {
                    $table->dropForeign('devices_user_id_foreign');
                    $table->dropColumn('user_id');
                });
            } else {
                echo "User_id column doesn't exsists";
            }
        } else {
            echo "No devices table";
        }
    }
}
