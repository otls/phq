<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configs', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id', false, true)->nullable();
            $table->string('conf_name', 255);
            $table->text('conf_note')->nullable();
            $table->integer('conf_ph_min');
            $table->integer('conf_ph_max');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id', 'configs_user_id_foreign')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('configs')) {
            Schema::table('configs', function (Blueprint $table) {
                $table->dropForeign('configs_user_id_foreign');
            });
            Schema::dropIfExists('configs');
        }
    }
}
