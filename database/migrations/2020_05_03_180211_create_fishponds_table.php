<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFishpondsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fishponds', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('device_id', false, true)->nullable();
            $table->bigInteger('config_id', false, true)->nullable();
            $table->string('fspnd_name', 255)->nullable();
            $table->integer('fspnd_ph')->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('device_id', 'fishponds_device_id_foreign')->references('id')->on('devices')->onDelete('cascade');
            $table->foreign('config_id', 'fishponds_config_id_foreign')->references('id')->on('configs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('fishponds')) {
            Schema::table('fishponds', function (Blueprint $table) {
                $table->dropForeign('fishponds_device_id_foreign');
                $table->dropForeign('fishponds_config_id_foreign');
            });
            Schema::dropIfExists('fishponds');
        }
    }
}
