<?php
return [
    'roles' => [
        'admin' => ['role_code' => 100, 'role_name' => "Administrator"],
        'user'  => ['role_code' => 110, 'role_name' => 'Member'],
        'operator'  => ['role_code' => 120, 'role_name' => 'Operator'],
    ],
    'pagination' => 10
];
