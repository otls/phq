let datatable = null;
let _token = null;

$(async () => {
    _token = await getCsrfToken();
    let datatableConfig = {
        serverSide: true,
        ajax: {
            type: "POST",
            url: './user/data',
            data: {
                _token: _token
            }
        },
        columns: [{
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                searchable: false,
                orderable: false
            },
            {
                data: "name",
                name: "users.name"
            },
            {
                data: "email",
                name: "users.email"
            },
            {
                data: "device_key",
                name: "devices.device_key",
                searchable: false,
                orderable: false,
            },
            {
                data: "fishpond_id",
                name: "fishponds.id",
                searchable: false,
                orderable: false,
            },
            {
                data: 'id',
                name: 'users.id',
                searchable: false,
                orderable: false,
                render: (value, param1, data) => {
                    let updateConfig = {
                        href: `/mimin/user/${value}/edit`,
                        title: "Rubah data ini",
                        class: 'text-light',
                        id: `update${value}`,
                    };
                    let deleteConfig = {
                        action: '/mimin/user/' + value,
                        token: $('meta[name=csrf-token]').attr('content'),
                        title: 'Hapus data ini',
                        class: '',
                        id: `delete${value}`,
                    }
                    return renderDatatableUpdateAndDeleteActionButton(updateConfig, deleteConfig);
                }
            }

        ]
    };
    datatable = createDatatable(datatableConfig);
});
