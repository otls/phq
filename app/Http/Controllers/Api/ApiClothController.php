<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Config;
use App\Traits\Api\UserTraitApi;
use Illuminate\Http\Request;

class ApiClothController extends Controller
{
    use UserTraitApi;
    protected $response = [
        'success' => true,
        'message' => "Operation Success!",
        'code' => 200
    ];

    protected function sendResponse($data = null)
    {
        $this->response = null != $data ? $data : $this->response;
        return response()->json($this->response, $this->response['code']);
    }

    protected function checkConfigOwner(Config $config)
    {
        if (!$this->is_config_owner($config)) {
            $this->response['success'] = false;
            $this->response['message'] = "You can't do something with this configuration";
            return false;
        }
        return true;
    }
}
