<?php

namespace App\Http\Controllers\Api\Devices;

use App\Http\Controllers\Api\ApiClothController;
use App\Http\Requests\MCFishpondMonitorStore;
use App\Models\Fishpond;
use Illuminate\Http\Request;

class McController extends ApiClothController
{
    public function fishpondMonitor(MCFishpondMonitorStore $request, Fishpond $fishpond)
    {
        try {
            $fishpond->fspnd_ph = $request->fspnd_ph;
            $fishpond->save();
            $this->response['message'] = true;
            $this->response['data'] = $fishpond->toArray();
        } catch (\Exception $th) {
            $this->response['message'] = $th->getMessage();
            $this->response['code'] = 500;
            $this->response['success'] = false;
        }
        return $this->sendResponse();
    }

    public function getConfig()
    {
        try {
            $configs = Fishpond::where('device_id', auth('device')->user()->id)->with('config')->get();
            $this->response['message'] = $configs;
        } catch (\Exception $e) {
            $this->response = ['message' => $e->getMessage(), 'success' => false, 'code' => 500];
        }
        return $this->sendResponse();
    }

    public function getConfigByFishpond(Fishpond $fishpond)
    {
        try {
            $this->response['message'] = $fishpond->load('config');
        } catch (\Exception $e) {
            $this->response = ['message' => $e->getMessage(), 'success' => false, 'code' => 500];
        }
        return $this->sendResponse();
    }
}
