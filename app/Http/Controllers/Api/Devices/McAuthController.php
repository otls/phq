<?php

namespace App\Http\Controllers\Api\Devices;

use App\Http\Controllers\Api\ApiClothController;
use App\Models\Device;
use Illuminate\Http\Request;

class McAuthController extends ApiClothController
{
    public function login(Request $request)
    {
        $request->validate([
            'device_key' => 'required|string|min:10|max:255'
        ]);
        $device = Device::where('device_key', $request->device_key)->first();
        if ($device and $token = auth('device')->login($device)) {
            $this->response['message'] = $token;
        } else {
            $this->response = ['message' => 'Sorry sorry aja nih gua mah ya bor yaa!', 'success' => false, 'code' => 401];
        }

        return $this->sendResponse();
    }
}
