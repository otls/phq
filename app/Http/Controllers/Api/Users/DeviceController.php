<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Api\ApiClothController;
use App\Models\Device;
use Illuminate\Http\Request;

class DeviceController extends ApiClothController
{
    public function index(Request $request, Device $device)
    {
        $data = Device::where('user_id', $request->user->id);
        $data = !$device->id ? $data->paginate(config('phq.pagination'))  : $device;
        $this->response['message'] = $data->load('fishponds.config');
        return $this->sendResponse();
    }
}
