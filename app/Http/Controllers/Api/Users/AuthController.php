<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Api\ApiClothController;
use App\User;
use Illuminate\Http\Request;

class AuthController extends ApiClothController
{

    public function login(Request $request)
    {
        $credential = $request->only(['email', 'password']);
        $credential['password'] = md5($credential['password']);
        // dd($credential);
        $data = User::where('email', $credential['email'])->where('password', $credential['password'])->first();
        // dd($data);

        if ($data) {
            $token = auth('api')->login($data);
            $this->response['message'] = $token;
            return $this->sendResponse();
        }
        $this->response = ['message' => 'Unauthorized', 'success' => false, 'code' => 401];
        return $this->sendResponse();
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $this->response['message'] =  response()->json(auth('api')->user());
        return $this->sendResponse();
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        auth('api')->logout();
        $this->response['message'] = 'Successfully logged out';
        return $this->sendResponse();
    }

    // /**
    //  * Refresh a token.
    //  *
    //  * @return \Illuminate\Http\JsonResponse
    //  */
    // public function refresh()
    // {
    //     $this->response['message'] = auth('api')->refresh();
    //     return $this->sendResponse();
    // }
}
