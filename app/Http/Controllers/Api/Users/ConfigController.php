<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Api\ApiClothController;
use App\Http\Requests\StoreConfig;
use App\Models\Config;
use Illuminate\Http\Request;

class ConfigController extends ApiClothController
{
    public function __construct()
    {
        $this->middleware('config.owner')->except(['index', 'store']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $this->response['message'] = Config::where('user_id', $request->user->id)->paginate(config('phq.pagination'));
        } catch (\Exception $e) {
            $this->response['message'] = $e->getMessage();
            $this->response['code'] = 500;
        }

        return $this->sendResponse();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreConfig $request)
    {
        $data = $request->validated();
        $data['user_id'] = auth('api')->user()->id;

        try {
            $config = Config::create($data);
            $this->response['message'] = $config;
        } catch (\Exception $e) {
            $this->response['message'] = $e->getMessage();
            $this->response['code'] = 500;
        }
        return $this->sendResponse();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Config $config)
    {
        try {
            $this->response['message'] = $config->load('fishponds.device');
        } catch (\Exception $e) {
            $this->response['message'] = $e->getMessage();
            $this->response['code'] = 500;
        }
        return $this->sendResponse();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreConfig $request, Config $config)
    {
        $data = $request->validated();
        try {
            $config = Config::find($config->id)->update($data);
            $this->response['message'] = "Update success!";
        } catch (\Exception $e) {
            $this->response['message'] = $e->getMessage();
            $this->response['code'] = 500;
        }
        return $this->sendResponse();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Config $config)
    {
        try {
            $config->delete();
            $this->response['message'] = "Delete Success!";
        } catch (\Exception $e) {
            $this->response['message'] = $e->getMessage();
            $this->response['code'] = 500;
        }
        return $this->sendResponse();
    }
}
