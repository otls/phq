<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Api\ApiClothController;
use App\Models\Config;
use App\Models\Fishpond;
use Illuminate\Http\Request;

class FishpondController extends ApiClothController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $this->response['message'] = Fishpond::with(['device.user', 'config'])->whereHas('device.user', function ($query) {
                $query->where('users.id', '=', auth('api')->user()->id);
            })->paginate(config('phq.pagination'));
        } catch (\Exception $e) {
            $this->response['message'] = $e->getMessage();
            $this->response['code'] = 500;
        }
        return $this->sendResponse();
    }

    public function updateConfig(Request $request, Fishpond $fishpond, Config $config)
    {
        try {
            $fishpond->config_id = $config->id;
            $fishpond->save();
            $this->response['message'] = "Update fishpond config success!";
            $this->response['data'] = $fishpond->with('config');
        } catch (\Exception $e) {
            $this->response['message'] = $e->getMessage();
            $this->response['code'] = 500;
        }

        return $this->sendResponse();
    }

    public function show(Fishpond $fishpond)
    {
        try {
            $this->response['message'] = $fishpond->load('config');
        } catch (\Exception $e) {
            $this->response['message'] = $e->getMessage();
            $this->response['code'] = 500;
        }
        return $this->sendResponse();
    }
}
