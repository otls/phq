<?php

namespace App\Http\Controllers\Admin;

use App\Models\Config;
use App\Models\Device;
use App\Models\Fishpond;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class UserController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admins.user');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admins.form_user');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'email|required|unique:users,email',
            'password' => 'required',
            'device_key' => 'required|unique:devices,device_key',
            'fishpond_id' => 'required|unique:fishponds,id'
        ]);

        try {
            $user = [
                'name' => $request->name,
                'email' => $request->email,
                'password' => md5($request->password),
                'role_code' => 110
            ];

            $user_id = User::insertGetId($user);
            $device = [
                'device_key' => $request->device_key,
                'user_id' => $user_id
            ];

            $device_id = Device::insertGetId($device);
            $config = [
                'conf_name' => 'Default',
                'conf_note' => 'Default',
                'conf_ph_min' => 7,
                'conf_ph_max' => 7,
                'user_id' => $user_id,
            ];
            $config_id = Config::insertGetId($config);
            $id = $request->fishpond_id;
            $fishpond = [
                'device_id' => $device_id,
                'fspnd_name' => $request->fspnd_name,
                'config_id' => $config_id,
                'id' => $id
            ];
            Fishpond::create($fishpond);
            $response = [
                'success' => true,
                'message' => "Berhasil Memasukan data!"
            ];
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage()
            ];
        }
        return redirect()->route('user.index')->with($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $data = $user;
        // dd($data);
        return view('admins.form_user', ['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => 'required',
            'email' => ['required', 'email', Rule::unique('users', 'email')->ignore($user)],
            'fspnd_name' => 'required'
        ]);

        $user->email = $request->email;
        $user->name = $request->name;
        $user->password = $request->password ? md5($request->password) : $user->password;

        $user->save();
        $user->device->fishpond->fspnd_name = $request->fspnd_name;
        return redirect()->route('user.index')->with(['message' => "berhasil menyunting data", 'success' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, User $user)
    {
        $user->forceDelete();
        return response()->json(['message' => 'berhasil menghapus data', 'success' => true, 'code' => 200], 200);
    }

    public function data(Request $request)
    {
        $user = User::selectRaw('users.*, devices.device_key, fishponds.id as fishpond_id')
            ->leftJoin('devices', 'devices.user_id', 'users.id')
            ->leftJoin('fishponds', 'fishponds.device_id', 'devices.id')
            ->leftJoin('configs', 'configs.id', 'fishponds.config_id')
            ->where('users.role_code', config('phq.roles.user.role_code'));
        return $this->datatables($user);
    }
}
