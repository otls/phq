<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class AdminController extends Controller
{
    protected function datatables($model)
    {
        return DataTables::eloquent($model)
            ->addIndexColumn()
            ->toJson();
    }
}
