<?php

namespace App\Http\Requests;

use App\Models\Config;
use App\Traits\Api\UserTraitApi;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

class StoreConfig extends FormRequest
{
    use UserTraitApi;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // dd($config);
        if (null == $this->config) {
            return true;
        }
        return $this->is_config_owner($this->config);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'conf_name' => 'required|string|max:255',
            'conf_note' => 'string|nullable',
            'conf_ph_min' => 'required|numeric|max:14',
            'conf_ph_max' => 'required|numeric|max:14|gte:conf_ph_min',
        ];
    }

    public function attributes()
    {
        return [
            'conf_name' => "Configuration name",
            'conf_note' => "Configuration Note",
            'conf_ph_min' => "Minimum pH",
            'conf_ph_max' => "Maximum pH",
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();

        throw new HttpResponseException(
            response()->json(['errors' => $errors], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
