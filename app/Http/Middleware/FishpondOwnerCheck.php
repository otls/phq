<?php

namespace App\Http\Middleware;

use App\Traits\Api\UserTraitApi;
use Closure;

class FishpondOwnerCheck
{
    use UserTraitApi;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->fishpond and $this->is_fishpond_owner($request->fishpond)) {
            # code...
            return $next($request);
        }

        return response()->json(['success' => false, 'message' => "you are not allowed with this fishpond"]);
    }
}
