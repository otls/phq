<?php

namespace App\Http\Middleware;

use Closure;

class JWTAuthCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $user = auth('api')->userOrfail();
            $request->user = $user;
            //dd($user);
            return $next($request);
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response(['success' => false, "message" => "You are not allowed", "code" => 401], 401);
        }
    }
}
