<?php

namespace App\Http\Middleware;

use App\Traits\Api\MCTraitApi;
use Closure;

class MCFishpondOwnerCheck
{
    use MCTraitApi;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->fishpond and $this->is_fishpond_owner($request->fishpond)) {
            # code...
            return $next($request);
        } else {

            return response()->json(['success' => false, 'message' => "We are watching you!", 'code' => 401], 401);
        }
        //return $next($request);
    }
}
