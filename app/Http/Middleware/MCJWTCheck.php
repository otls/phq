<?php

namespace App\Http\Middleware;

use Closure;

class MCJWTCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $mc = auth('device')->userOrFail();
            $request->user_device = $mc;
            return $next($request);
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response(['success' => false, "message" => "You are not allowed", "code" => 401], 401);
        }
    }
}
