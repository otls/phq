<?php

namespace App\Http\Middleware;

use App\Traits\Api\UserTraitApi;
use Closure;

class ConfigOwnerCheck
{
    use UserTraitApi;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->config and $this->is_config_owner($request->config)) {
            # code...
            return $next($request);
        }

        return response()->json(['success' => false, 'message' => "you are not allowed with this configuration"]);
    }
}
