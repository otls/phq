<?php

namespace App\Http\Middleware;

use App\Traits\Api\UserTraitApi;
use Closure;

class DeviceOwnerCheck
{
    use UserTraitApi;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->device and $this->is_device_owner($request->device)) {
            # code...
            return $next($request);
        }

        return response()->json(['success' => false, 'message' => "you are not allowed with this device"]);
    }
}
