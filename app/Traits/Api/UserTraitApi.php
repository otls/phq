<?php

namespace App\Traits\Api;

use App\Models\Config;
use App\Models\Device;
use App\Models\Fishpond;

/**
 * Something connected with user
 */
trait UserTraitApi
{
    public function is_config_owner(Config $config)
    {
        return auth('api')->user()->id === $config->user->id;
    }

    public function is_fishpond_owner(Fishpond $fishpond)
    {
        return auth('api')->user()->id === $fishpond->device->user->id;
    }

    public function is_device_owner(Device $device)
    {
        return auth('api')->user()->id === $device->user->id;
    }

    public function is_fishpon_and_config_owner(Fishpond $fishpond, Config $config)
    {
        return $this->is_fishpond_owner($fishpond) and $this->is_config_owner($config);
    }
}
