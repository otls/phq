<?php

namespace App\Traits\Api;

use App\Models\Fishpond;

/**
 * Trait for microcontroller
 */
trait MCTraitApi
{
    public function is_fishpond_owner(Fishpond $fishpond)
    {
        return auth('device')->user()->id === $fishpond->device_id;
    }
}
