<?php

namespace App\Console\Commands;

use App\Models\Ipddns;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class UpdateIpDDNS extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ipddns:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update IP to ddns dynus';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $trial = 0;
        start:
        // prepare data
        $urlIPChecker = 'https://api.ipify.org?format=json';
        $dynusHeaders = [
            'Accept' => 'application/json',
            'API-KEY' => '6YTceV34U7Y56634V5b3dWWcWa2We6U5'
        ];
        $dynusURL = 'https://api.dynu.com/nic/update';
        $dynusParams = [
            'hostname' => 'qdev.mywire.org',
            'myip' => '',
            'password' => '3454a799ab814617a605858dc8029e1d',
            'username' => 'qdev919'
        ];
        // get this ip
        $response = Http::get($urlIPChecker);

        if ($response->successful()) {
            $response = $response->json();
            $ip = $response['ip'];
            $lastIp = Ipddns::latest()->first();
            if (!$lastIp or (isset($lastIp->ip) and ($lastIp->ip != $ip))) {
                $dynusTrials = 0;
                dynusStart:
                $dynusParams['myip'] = $ip;
                $updateIP = Http::withHeaders($dynusHeaders)
                ->get($dynusURL, $dynusParams);
                if ($updateIP->successful()) {
                    Ipddns::create(['ip' => $ip]);
                    Log::channel('ipddns')->info('IP Successfully UPDATED: ' . $ip);
                    $this->info("SUCCESSFULLY UPDATED IP!");
                }else{
                    if ($dynusTrials < 3) {
                        $dynusTrials += 1;
                        goto dynusStart;
                    }
                    Log::channel('ipddns')->error('IP Failed Updated: dynus - ' . $updateIP->body());
                    $this->error("ERROR UPDATED IP AT DYNUS!");
                }
            }else{
                Log::channel('ipddns')->info('no need to update: ' . $ip);
                $this->info("no need to update");
            }

        }else{
            Log::channel('ipddns')->error('Error IPFY AT TRIAL: ' . $trial);
            $this->error("ERROR IPFY!");
            $trial++;
            sleep(10);
            if ($trial < 3) {
                # code...
                goto start;
            }
        }

    }
}
