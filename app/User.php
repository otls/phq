<?php

namespace App;

use App\Models\Device;
use App\Models\Role;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'deleted_at', 'updated_at', 'created_at', 'email_verified_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];



    public function getPictureAttribute()
    {
        $pp = asset(('images/noimage.jpg'));
        if (null != $this->profile_picture) {
            $pp = asset("profiles/{$this->profile_picture}");
        }
        return $pp;
    }

    // public function getDeviceCountAttribute()
    // {
    //     return $this->devices->count();
    // }
    // public function getFishpondCountAttribute()
    // {
    //     $device =  Device::where('user_id', $this->id)->first();
    //     //dd($device);
    //     return $device->fishponds->count();
    // }

    /**
     * JWT AUTH. Get the identifier that will be stored in the subject claim of the JWT.
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /** Relationship */

    public function device()
    {
        return $this->hasOne(Device::class, 'user_id');
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_code', 'role_code');
    }
}
