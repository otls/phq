<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Config extends Model
{
    use SoftDeletes;

    public $timestamps = true;
    protected $guarded = ['id', 'created_at', 'deleted_at', 'updated_at'];
    protected $hidden = [
        'created_at', 'deleted_at', 'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function fishponds()
    {
        return $this->hasMany(Fishpond::class);
    }
}
