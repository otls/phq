<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Device extends Authenticatable implements JWTSubject
{
    use SoftDeletes, Notifiable;

    public $timestamps = true;
    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $hidden = [
        'created_at', 'deleted_at', 'updated_at'
    ];
    // protected $appends = [
    //     'count_fishponds'
    // ];

    public function getCountAttribute()
    {
        return $this->fishponds->count();
    }

    /**
     * JWT AUTH. Get the identifier that will be stored in the subject claim of the JWT.
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function fishpond()
    {
        return $this->hasOne(Fishpond::class, 'device_id');
    }

    // public function getCountFishpondsAttribute()
    // {
    //     return $this->fishponds->count();
    // }
}
