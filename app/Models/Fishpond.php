<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fishpond extends Model
{
    use SoftDeletes;

    public $timestamps = true;
    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];
    protected $hidden = [
        'created_at', 'deleted_at', 'updated_at'
    ];


    public function config()
    {
        return $this->belongsTo(Config::class, "config_id");
    }

    public function device()
    {
        return $this->belongsTo(Device::class, 'device_id');
    }
}
