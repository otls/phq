@extends('layouts.app')

@section('title')
    Tambah Pengguna
@endsection

@section('pageScript')
    {{asset('js/pages/adm_user_form.js')}}
@endsection

@section('content')
{{-- {{ dd($data->device) }} --}}
    <section class=" section-dashboard bg-light" style="min-height: 90vh">
        <div class="container">
            <h4>{{!isset($data->id) ? "Tambah Pengguna" : "Rubah data $data->name"}}</h4>
            <hr>
            <div class="row">
                <div class="col-12">
                    <div class="card border-0 shadow">
                        <div class="card-body">
                            <form action="{{isset($data->id) ? route('user.update', $data->id) : route('user.store')}}" id="form-user" method="POST">
                                @csrf
                                @if (isset($data->id))
                                    @method('PUT')
                                @endif
                                <div class="form-group">
                                    <label for="name">Nama</label>
                                    <input type="text" name="name" id="name" placeholder="Masukan nama pengguna" maxlength="255" required value="{{old('name') ? old('name') : (isset($data->id) ? $data->name : null)}}" class="form-control @error('name') is-invalid @enderror">
                                    @error('name')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" id="email" placeholder="Masukan email pengguna" maxlength="50" required value="{{old('email') ? old('email') : (isset($data->id) ? $data->email : null)}}" class="form-control @error('email') is-invalid @enderror">
                                    @error('email')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="text" name="password" id="password" placeholder="{{!isset($data->id) ? "Masukan password pengguna (min 6 karakter)" : "Kosongkan jika tidak ingin merubah password"}}" {{!isset($data->id) ? 'required' : null}} minlength="6" maxlength="255"  class="form-control @error('password') is-invalid @enderror">
                                    @error('password')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="password">ID Device</label>
                                    <input type="text" readonly value="{{old('device_key') ? old('device_key') : (isset($data->id) ? $data->device->device_key : time() . rand(0, 10))  }}" name="device_key" id="device_key" placeholder="{{!isset($data->id) ? "Masukan device_key pengguna (min 6 karakter)" : "Kosongkan jika tidak ingin merubah device_key"}}" {{!isset($data->id) ? 'required' : null}} minlength="6" maxlength="255"  class="form-control @error('device_key') is-invalid @enderror">
                                    @error('device_key')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group d-none">
                                    <label for="fishpond">ID Kolam</label>
                                    <input type="text" readonly name="fishpond_id" value="{{old('fishpond_id') ? old('fishpond_id') : ((isset($data->id)) ? $data->device->fishpond->id : time() . rand(0, 10))  }}" id="fishpond_id"  minlength="6" maxlength="255"  class="form-control @error('fishpond_id') is-invalid @enderror">
                                    @error('fishpond_id')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="fishpond">Nama Kolam</label>
                                    <input required type="text"  name="fspnd_name" value="{{old('fspnd_name') ? old('fspnd_name') : (isset($data->id) ? $data->device->fishpond->fspnd_name : null)}}" id="fspnd_name" @if(isset($data->id)) readonly @endif   maxlength="255"  class="form-control @error('fspnd_name') is-invalid @enderror">
                                    @error('fspnd_name')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group"><button type="submit" class="btn btn-primary btn-block">Simpan</button></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @if ($errors->any())
        <script>
            $(() => {
                console.log(`<?php var_dump($errors) ?>`);
            })
        </script>
    @endif
@endsection
