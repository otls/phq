@extends('layouts.app')

@section('title')
    User Management
@endsection

@section('script')
    <script src="{{asset('js/pages/user.js')}}"></script>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 flex-row">
            <div class="row mb-3">
                <div class="col-6">
                    <h3>Data User</h3>
                </div>
                <div class="col-6">
                    <a href="{{ route('user.create') }}" class="btn btn-primary float-right">Tambah</a>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card border-0 shadow">
                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Device Key</th>
                                <th>ID Kolam</th>
                                <th>#</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
