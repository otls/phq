<li class="nav-item">
  <a href="{{route('home')}}" class="nav-link {{Request::url() == route('home') ? 'active' : ''}}">
    <i class="nav-icon fas fa-tachometer-alt"></i>
    <p>
      Dashboard
    </p>
  </a>
</li>
<li class="nav-item">
  <a href="{{route('user.index')}}" class="nav-link {{Request::is('mimin/user*') ? 'active' : ''}}">
    <i class="nav-icon fas fa-users"></i>
    <p>
      Users
    </p>
  </a>
</li>
