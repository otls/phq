require('./bootstrap');
// notification toastr
window.notify = (message = "Something happened!", type = "info", title = "INFO") => {
    let toastr = require('toastr/toastr');
    switch (type) {
        case 'success':
            toastr.success(message, title);
            break;
        case 'info':
            toastr.info(message, title);
            break;
        case 'warning':
            toastr.warning(message, title);
            break;
        case 'error':
            toastr.error(message, title);
            break;
        default:
            toastr.info(message, title);
            break;
    }
}

$(async () => {
    window.csrfToken = await getCsrfToken();
    $('.strong').html(`Copyright &copy; 2014 - The end of the day | <a href="https://www.facebook.com/arie.umarella" target="_blank">Maestro Arie Umarella</a>.`)
});

window.createDatatable = async (config, selector = '.table') => {
    let defaultConf = {
        searching: true,
        paging: true,
        autoWidth: true,
        ordering: true,
        searching: true,
        processing: true,
        pagination: true,
    };
    config = {
        ...defaultConf,
        ...config
    };
    let dw = config.drawCallback;
    config.drawCallback = () => {
        if (typeof dw === 'function') {
            dw();
        }
        datatablesActionListener();
    }
    let datatable = window.datatable = $(selector).DataTable(config);
    return datatable;
}

window.renderDatatableUpdateAndDeleteActionButton = (
    updateConfig = {
        href: '/',
        title: "Rubah data ini",
        class: '',
        id: '',
    }, deleteConfig = {
        action: '/',
        token: '',
        title: 'Hapus data ini',
        class: '',
        id: '',
    }
) => {
    let deleteButtonId = deleteConfig.id ? `id="${deleteConfig.id}"` : null;
    let updateButtonId = updateConfig.id ? `id="${updateConfig.id}"` : null;

    let updateButton = `<a href="${updateConfig.href}" ${updateButtonId} data-toggle="tooltip" data-placement="top" title="${updateConfig.title}" class="btn btn-sm btn-warning mr-2 datatable-update-button ${updateConfig.class}"><i class="fas fa-cog"></i></a>`;
    let deleteButton = `
        <form action="${deleteConfig.action}" ${deleteButtonId} method="POST" class="datatable-delete-button ${deleteConfig.class}">
                <input type="hidden" name="_method" value="DELETE" />
                <input type="hidden" name="_token" value="${deleteConfig.token}" />
                <button data-toggle="tooltip" data-placement="top" title="${deleteConfig.title}" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></button>
        </form>
    `;
    return `<div class="row">${updateButton + deleteButton}</div>`;
}

window.datatablesActionListener = () => {
    let Swal = require('sweetalert2/dist/sweetalert2.all');
    $('.datatable-delete-button').submit(e => {
        e.preventDefault();
        Swal.fire({
            title: 'Apakah anda yakin ingin melanjutkan proses ini?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, hapus!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "DELETE",
                    url: $(e.currentTarget).attr('action'),
                    data: $(e.currentTarget).serialize(),
                    dataType: "json",
                    success: function (response) {
                        notify(response.message, 'success', response.title);
                        setTimeout(() => {
                            window.location.reload();
                        }, 1000);
                    },
                    error: (xhr, status, error) => {
                        notify(response.message, response.type, response.title);
                    }
                });
            }
        });

    });
}

window.getCsrfToken = async () => {
    let token = await $('meta[name=csrf-token]').attr('content');
    return token;
}
